"Export On Save" addon for Godot
================================

"Export On Save" is a simple addon for Godot that will export your game every
time that you save!

Installation
------------

1. Download the source code.

2. Copy the `addons/export-on-save` directory into the `addons/` directory in
   your Godot project.

3. Go to **"Project"** -> **"Project Settings..."** -> **"Plugins"** and click
   "Enable" next to "Export On Save".

4. Close the **"Project Settings"** dialog (the setting described below won't
   appear until first closing the dialog).

5. Go to **"Project"** -> **"Project Settings..."** -> **"Editor"** and change
   the **"Export Preset On Save"** setting to the preset that you want to export.

Configuration
-------------

You can choose which export preset gets exported by going to **"Project"** ->
**"Project Settings..."** -> **"Editor"** and changing the **"Export Preset On
Save"** setting.

_(If a preset you just added isn't available in the dropdown, just save your
project, close the "Project Settings" dialog and open it again.)_


